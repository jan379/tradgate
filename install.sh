#!/usr/bin/env bash

mkdir -p ~/.local/share/icons/hicolor/48x48/apps
mkdir -p ~/.local/share/applications
mkdir -p ~/.local/bin
mkdir -p ~/.config/lightsoff

cp lightsoff ~/.local/bin/
cp lightsoff.desktop ~/.local/share/applications
cp logo.svg ~/.local/share/icons/hicolor/48x48/apps/lightsoff.svg
cp config/lightsoff.ini ~/.config/lightsoff/
