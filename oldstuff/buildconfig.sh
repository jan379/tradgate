
source ./secrets

# create main config
#
echo '[main]'
echo "user = \"${gateway_user}\""
echo "preshared_key = \"${gateway_id}\""
echo "url = \"${gateway_address}\""
# find out the super group that we will need further on
#
grouplist=$(coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15004" | jq '.[]')
for group in ${grouplist}; do
  description=$(coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15004/${group}" | jq -r '."9001"')
  case ${description} in
    "SuperGroup")
    echo "generic_scene_group_id = ${group}"
    supergroup=${group}
    ;;
  esac
done
echo
 
# create room (i.e group) mapping
#
echo '[rooms]'
for group in ${grouplist}; do
  description=$(coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15004/${group}" | jq -r '."9001"')
  echo "${group} = ${description}"
done
echo
  
# create a scene mapping
#
echo '[scenes]'
scenelist=$(coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15005/${supergroup}" | jq '.[]')
for scene in $scenelist; do
  description=$(coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15005/${supergroup}/$scene" | jq -r '."9001"')
  echo "${scene} = ${description}"
done
echo

# Device inventory 
#
echo '[devices]'
devicelist=$(coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15001" | jq '.[]')
for device in $devicelist; do
  description=$(coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15001/${device}" | jq -r '."9001"')
  echo "${device} = ${description}"
done
echo

