#!/bin/sh
# Version 1.4
# Zugangsdaten des Gateways
IP="coaps://192.168.1.228:5684/15001/"
KEY="1RK6TVz8R4rttrpz"
USER="jan379"
DEVICE="1"
DEVCOUNT=0;
declare -A ALLBULBS

# Alle Glühbirnen suchen
ALLDEV=`/usr/bin/coap-client-openssl -m get -u $USER -k $KEY $IP`
patDev='[0-9]{5}'
ALLDEVARRAY=(${ALLDEV//,/ })
for TESTDEV in "${ALLDEVARRAY[@]}"
do
	[[ $TESTDEV =~ $patDev ]]
	ACTDEV=${BASH_REMATCH[0]}
	if [ "$ACTDEV" != "" ] 
	then
		DEVDATA=`/usr/bin/coap-client-openssl -m get -u $USER -k $KEY $IP$ACTDEV`
		patBulb='\"3311\"\:'
		[[ $DEVDATA =~ $patBulb ]]
		if [ "${BASH_REMATCH[0]}" != "" ]
		then
			ALLBULBS[$DEVCOUNT,0]="$ACTDEV"
			patName='9001\"\:\"([^\"]*)'
			[[ $DEVDATA =~ $patName ]]
			ALLBULBS[$DEVCOUNT,1]="${BASH_REMATCH[1]}"
			patStatus='5850\"\:([0-9]),\"5851\"\:([0-9]*)'
			[[ $DEVDATA =~ $patStatus ]]
			ALLBULBS[$DEVCOUNT,2]="${BASH_REMATCH[1]}"
			ALLBULBS[$DEVCOUNT,3]="${BASH_REMATCH[2]}"
			DEVCOUNT=$((DEVCOUNT+1))
		fi

	fi
done

while [ "$DEVICE" != "" ]
do
DEVNUMBER=0;
DEVICESTRING="";
while [ "$DEVNUMBER" -lt "$DEVCOUNT" ]
do
	DEVICESTRING="$DEVICESTRING False $DEVNUMBER ${ALLBULBS[$DEVNUMBER,0]} ${ALLBULBS[$DEVNUMBER,1]} ${ALLBULBS[$DEVNUMBER,2]} ${ALLBULBS[$DEVNUMBER,3]}"
	DEVNUMBER=$((DEVNUMBER+1))
done

# Auswahl der zu steuernden Lampe
DEVICE=`zenity --title "Lampenauswahl" --text="Bitte Lampe wählen:" --width 600 --height 400 --list --column Auswahl --column Nr. --column Lampen-ID --column Standort --column Status --column Helligkeit $DEVICESTRING --radiolist`

# Überprüfen, ob eine Lampe ausgewählt und bestätigt oder die Auswahl abgebrochen wurde
if [ "$DEVICE" != "" ]
then

	# Aktuelle Helligkeit auslesen und Helligkeitsregler setzen
 	HELLVALUE=$((${ALLBULBS[$DEVICE,3]} * 100 / 254))

	# Auswahl der zu setzenden Helligkeit
	HELLIGKEIT=`zenity --scale --title="Helligkeitseinstellung" --text="Helligkeit der Lampe." --value=$HELLVALUE`

	# Überprüfen, ob eine Helligkeit ausgewählt und bestätigt oder die Auswahl abgebrochen wurde
	if [ "$HELLIGKEIT" != "" ]
	then
		HELLIGKEIT=$(($HELLIGKEIT * 254 / 100))

		/usr/bin/coap-client-openssl -m put -u $USER -k $KEY -e '{ "3311": [{ "5851": '$HELLIGKEIT' }] }'  $IP${ALLBULBS[$DEVICE,0]}
		ALLBULBS[$DEVICE,3]=$HELLIGKEIT
		if [ "$HELLIGKEIT" -eq "0" ]
		then
			ALLBULBS[$DEVICE,2]=0
		elif [ "$HELLIGKEIT" -gt "0" ]
		then
			ALLBULBS[$DEVICE,2]=1
		fi
		
	fi

fi
done
