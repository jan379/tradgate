#!/usr/bin/env bash

source secrets

create_inventory(){

  device_list=($(coap-client-openssl -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15001" | sed -e s/,/\ /g | tr -d '[,]'))
  sum_dev=${#device_list[@]}
  echo devices: $sum_dev
  step_width=$(( 100 / sum_dev))
  progress=0

  for device_id in ${device_list[@]}; do 
    echo $progress
    coap-client-openssl -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15001/${device_id}" >> inventory.json
    progress=$(( progress + step_width))
  done | zenity --title "Setup p lights system" --progress --percentage=$progress --text "Scanning ${sum_dev} devices..."
}

select_items(){
  array="$(cat inventory.json| jq 'false, ."9001", ."5750"' | sed s/^false/FALSE/g | tr "\n" " ")"
  echo $array
  zenity --list  --checklist --column "Select"  --column "Device name" --column "Status" ${array[@]}
}

if test -f inventory.json; then
  echo "Skip to create inventory, it exists already."
  select_items
else
  echo "Create inventory, it does not exist."
  create_inventory
fi  


