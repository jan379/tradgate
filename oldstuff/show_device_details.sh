source ./secrets
device_id="$1"
coap-client-gnutls -m get -u "${gateway_user}" -k "${gateway_id}" "${gateway_address}/15001/${device_id}"
